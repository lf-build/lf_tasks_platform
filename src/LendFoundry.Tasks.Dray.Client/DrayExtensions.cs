using LendFoundry.Foundation.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Tasks.Dray.Client
{
    public static class DrayExtensions
    {
        public static IServiceCollection AddDrayClient(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddSingleton<ITaskRunner>(p => new Dray(services.GetServiceClient(p, endpoint, port)));
            return services;   
        }
    }
}