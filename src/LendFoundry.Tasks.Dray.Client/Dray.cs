﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using RestSharp;

namespace LendFoundry.Tasks.Dray.Client
{
    public class Dray : ITaskRunner
    {
        public Dray(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public Task<bool> Run(ITask task)
        {
            var request = new RestRequest("/jobs", Method.POST);
            request.AddJsonBody(Job.Parse(task));
            return Client.ExecuteAsync(request);
        }
    }
}
