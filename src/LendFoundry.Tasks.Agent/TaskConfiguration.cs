using System;

namespace LendFoundry.Tasks.Agent
{
    public class TaskConfiguration
    {
        public bool Disabled { get; set; }
        public string CronExpression { get; set; }
    }
}