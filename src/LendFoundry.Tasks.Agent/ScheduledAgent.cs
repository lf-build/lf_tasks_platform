using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Tokens;
using Quartz;
using Quartz.Impl;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using System.Threading.Tasks;
#endif
using System.Collections.Generic;
using System.Threading;
using LendFoundry.Tenant.Client;
using System;

namespace LendFoundry.Tasks.Agent
{
    public abstract class ScheduledAgent : IAgent
    {
        protected ScheduledAgent(
            ITokenHandler tokenHandler, 
            IConfigurationServiceFactory configurationFactory, 
            ITenantTimeFactory tenantTimeFactory,
            ITenantServiceFactory tenantServiceFactory,
            ILoggerFactory loggerFactory,
            string taskName
            )
        {
            TokenHandler = tokenHandler;
            ConfigurationFactory = configurationFactory;
            TenantTimeFactory = tenantTimeFactory;
            TenantServiceFactory = tenantServiceFactory;
            LoggerFactory = loggerFactory;
            Logger = LoggerFactory.CreateLogger();
            TaskName = taskName;
        }

        private ITokenHandler TokenHandler { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }
        
        private ILoggerFactory LoggerFactory { get; }

        private ILogger Logger { get; }

        private string TaskName { get; }

        public void Execute()
        {
            SubscribeTenant();
        }

        public abstract void OnSchedule(string tenant);

        private IJobDetail CreateJob()
        {
            var data = new Dictionary<string, object>
            {
                {"agent", this}
            } as IDictionary<string, object>;

            return JobBuilder
                .Create<AgentJob>()
                .SetJobData(new JobDataMap(data))
                .Build();
        }

        private ITrigger CreateTrigger(string tenant, string cronExpression, ITenantTime tenantTime)
        {
            var expression = new CronExpression(cronExpression) {TimeZone = tenantTime.TimeZone};
            var schedule = CronScheduleBuilder.CronSchedule(expression);
            var triggerBuilder = TriggerBuilder.Create().WithSchedule(schedule);
            var trigger = triggerBuilder.UsingJobData("tenant",tenant).Build();
            return trigger;
        }

        private void SubscribeTenant()
        {
            var emptyReader = new StaticTokenReader(string.Empty);
            var tenantService = TenantServiceFactory.Create(emptyReader);
            var tenants = tenantService.GetActiveTenants();
            var scheduler = new StdSchedulerFactory().GetScheduler().Result;
            tenants.ForEach(tenant =>
            {
                try
                {
                    var token = TokenHandler.Issue(tenant.Id, TaskName);
                    var reader = new StaticTokenReader(token.Value);
                    var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                    var configurationService = ConfigurationFactory.Create<TaskConfiguration>(TaskName, reader);
                    var configuration = configurationService.Get();
                    if(configuration == null)
                        throw new ArgumentException($"Configuration not found for task: {TaskName} and tenant: {tenant.Id}");
                    if(configuration.Disabled)
                        return;
                    if(string.IsNullOrWhiteSpace(configuration.CronExpression))
                        throw new ArgumentException($"Cron expression not found for task: {TaskName} and tenant: {tenant.Id}");
                    scheduler.ScheduleJob(CreateJob(), CreateTrigger(tenant.Id,configuration.CronExpression, tenantTime));
                }
                catch (Exception ex)
                {
                    Logger.Error($"Error while subscribing tenant {tenant.Id}", ex);
                }
            });
            scheduler.Start();
            while (!scheduler.IsShutdown) Thread.Sleep(100);
        }

        private class AgentJob : IJob
        {
#if DOTNET2
            public Task Execute(IJobExecutionContext context)
            {
                return Task.Run(() =>
                {
                    var tenantId = context.Trigger.JobDataMap.Get("tenant");
                    var agent = context.MergedJobDataMap.Get("agent") as ScheduledAgent;
                    agent?.OnSchedule(Convert.ToString(tenantId));
                });
            }
#else
            public void Execute(IJobExecutionContext context)
            {
                var agent = context.MergedJobDataMap.Get("agent") as ScheduledAgent;
                agent?.OnSchedule();
            }
#endif
        }
    }
}