﻿using System;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System.Threading;
using LendFoundry.Tenant.Client;
using LendFoundry.Configuration;

namespace LendFoundry.Tasks.Agent
{
    public abstract class EventBasedAgent : IEventBasedAgent
    {
        private const string TenantEnvironmentVariable = "TASK_TENANT";

        private static readonly ManualResetEvent _waiter = new ManualResetEvent(false);

        protected EventBasedAgent(IEventHubClientFactory eventHubFactory, ITenantServiceFactory tenantServiceFactory, IConfigurationServiceFactory configurationFactory, string eventName)
        {
            EventHubFactory = eventHubFactory;
            EventName = eventName;
            ConfigurationFactory = configurationFactory;
        }

        public EventBasedAgent(IEventHubClientFactory eventHubFactory, ITenantServiceFactory tenantServiceFactory, IConfigurationServiceFactory configurationFactory, string taskName, string eventName, ITokenHandler tokenHandler, ILoggerFactory loggerFactory) : this(eventHubFactory, tenantServiceFactory, configurationFactory, eventName)
        {
            TaskName = taskName;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            ConfigurationFactory = configurationFactory;
        }

        protected IEventHubClientFactory EventHubFactory { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        protected ITenantServiceFactory TenantServiceFactory { get; }

        protected ITokenHandler TokenHandler { get; }

        protected ILoggerFactory LoggerFactory{ get; }

        private string TaskName { get; }

        private string EventName { get; }

        public void Execute()
        {
            var logger = LoggerFactory.CreateLogger();
            try
            {
                logger.Info("Starting Agent");
                
                var emptyReader = new StaticTokenReader(string.Empty);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();

                tenants.ForEach(tenant =>
                {
                    try
                    {
                        var token = TokenHandler.Issue(tenant.Id, TaskName);
                        var reader = new StaticTokenReader(token.Value);
                        var configurationService = ConfigurationFactory.Create<TaskConfiguration>(TaskName, reader);
                        var configuration = configurationService.Get();
                        if(configuration == null)
                            throw new ArgumentException($"Configuration not found for task: {TaskName} and tenant: {tenant.Id}");
                        if(configuration.Disabled)
                            return;
                        var hub = EventHubFactory.Create(reader);
                        logger.Info($"Subscribing to {EventName} to {tenant.Id}...");
                        hub.On(EventName, @event =>
                        {
                            logger.Info($"Event {@event.Name} received from {@event.TenantId}, starting execution...", @event);
                            Execute(@event);
                            logger.Info("Execution completed");
                        });
                        
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"Error while subscribing tenant {tenant.Id}", ex);
                    }
                });
                logger.Info("Subscription active");
                logger.Info("Agent started");
                Console.CancelKeyPress += (e, a) => _waiter.Set();
                _waiter.WaitOne();
            }
            catch (Exception ex)
            {
                logger.Error($"\nAgent raise an error: {ex.Message} \n", ex);
                Execute();
            }
        }

        public abstract void Execute(EventInfo @event);
    }
}