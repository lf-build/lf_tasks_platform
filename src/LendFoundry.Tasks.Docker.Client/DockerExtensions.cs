using System;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Tasks.Docker.Client
{
    public static class DockerExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddDockerRunner(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ITaskRunnerFactory>(p => new DockerRunnerFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ITaskRunnerFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddDockerRunner(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<ITaskRunnerFactory>(p => new DockerRunnerFactory(p, uri));
            services.AddTransient(p => p.GetService<ITaskRunnerFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddDockerRunner(this IServiceCollection services)
        {
            services.AddTransient<ITaskRunnerFactory>(p => new DockerRunnerFactory(p));
            services.AddTransient(p => p.GetService<ITaskRunnerFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}