using System;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Tasks.Docker.Client
{
    public class DockerRunnerFactory : ITaskRunnerFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public DockerRunnerFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public DockerRunnerFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; set; }
        private Uri Uri { get; }

        public ITaskRunner Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("docker_runner");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new DockerRunner(client);
        }
    }
}