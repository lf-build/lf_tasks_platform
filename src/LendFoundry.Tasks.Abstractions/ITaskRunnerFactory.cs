using LendFoundry.Security.Tokens;

namespace LendFoundry.Tasks
{
    public interface ITaskRunnerFactory
    {
        ITaskRunner Create(ITokenReader reader);
    }
}