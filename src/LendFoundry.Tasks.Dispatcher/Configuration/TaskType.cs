namespace LendFoundry.Tasks.Dispatcher.Configuration
{
    public enum TaskType
    {
        Scheduled,
        EventBased
    }
}