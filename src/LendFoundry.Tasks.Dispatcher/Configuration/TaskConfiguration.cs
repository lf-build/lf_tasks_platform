﻿using System.Collections.Generic;

namespace LendFoundry.Tasks.Dispatcher.Configuration
{
    public class TaskConfiguration : ITaskConfiguration
    {
        public TaskConfiguration()
        {
            Definitions = new List<Task>();
        }

        public List<Task> Definitions { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
