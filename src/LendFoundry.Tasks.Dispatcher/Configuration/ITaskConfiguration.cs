﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Tasks.Dispatcher.Configuration
{
    public interface ITaskConfiguration : IDependencyConfiguration
    {
        List<Task> Definitions { get; set; }
    }
}