﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Tasks.Dispatcher
{
    public class EmptyTokenReader : ITokenReader
    {
        public string Read()
        {
            return string.Empty;
        }
    }
}