using System;


namespace LendFoundry.Tasks.Dispatcher
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "tasks";
    }
}