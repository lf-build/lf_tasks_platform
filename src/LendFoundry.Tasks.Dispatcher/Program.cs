﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Dispatcher.Configuration;
using LendFoundry.Tasks.Docker.Client;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Tasks.Dispatcher
{
    public class Program : DependencyInjection
    {
        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTenantService();
            services.AddSingleton<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddDockerRunner();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddEventHub(Settings.ServiceName);            
            services.AddConfigurationService<TaskConfiguration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<TaskConfiguration>(Settings.ServiceName);
           
            services.AddSingleton<ITaskDispatcher, TaskDispatcher>();
            services.AddSingleton(p => p);
            return services;
        }

        public void Main(string[] args)
        {
            Provider.GetService<ITaskDispatcher>().Start();
        }
    }
}